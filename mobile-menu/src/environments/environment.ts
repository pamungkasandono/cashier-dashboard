// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyC-2JKlYnAEHdU4v5gJe3XXkWLG1xLxuc0',
    authDomain: 'cashier-8b8c1.firebaseapp.com',
    projectId: 'cashier-8b8c1',
    storageBucket: 'cashier-8b8c1.`appspot.com',
    messagingSenderId: '422015916374',
    appId: '1:422015916374:web:cbd47a849accf75b425ef7',
    measurementId: 'G-J1GDLDJCE7',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
