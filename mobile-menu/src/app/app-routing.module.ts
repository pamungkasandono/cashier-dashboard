import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./home/home.module').then((m) => m.HomePageModule),
  },
  {
    path: 'detail',
    loadChildren: () =>
      import('./cart-details/cart-details.module').then(
        (m) => m.CartDetailsPageModule
      ),
  },
  {
    path: 'finish-order',
    loadChildren: () => import('./finish-order/finish-order.module').then( m => m.FinishOrderPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
