import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CartDetailsPageRoutingModule } from './cart-details-routing.module';

import { CartDetailsPage } from './cart-details.page';
import { MakananMinumanListComponent } from './makanan-minuman-list/makanan-minuman-list.component';
import { PaketListComponent } from './paket-list/paket-list.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CartDetailsPageRoutingModule,
  ],
  declarations: [
    CartDetailsPage,
    MakananMinumanListComponent,
    PaketListComponent,
    // MakananComponent,
    // MinumanComponent,
    // ListPaketComponent,
  ],
})
export class CartDetailsPageModule {}
