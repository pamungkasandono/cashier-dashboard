import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-makanan-list',
  templateUrl: './makanan-minuman-list.component.html',
  styleUrls: ['./makanan-minuman-list.component.scss'],
})
export class MakananMinumanListComponent implements OnInit {
  @Input() item: any;
  @Output() plus = new EventEmitter<string>();
  @Output() minus = new EventEmitter<string>();

  constructor() {}

  ngOnInit() {}

  btnPlus(id: string): void {
    this.plus.emit(id);
  }

  btnMinus(id: string): void {
    this.minus.emit(id);
  }
}
