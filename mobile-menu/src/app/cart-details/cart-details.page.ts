import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { MenuModel } from '../model/menu.model';
import { ApiService } from '../services/api.service';
import { CartService } from '../services/cart.service';

import firebase from 'firebase/compat/app';
import 'firebase/compat/firestore';

var firebaseConfig = {
  apiKey: 'AIzaSyC-2JKlYnAEHdU4v5gJe3XXkWLG1xLxuc0',
  authDomain: 'cashier-8b8c1.firebaseapp.com',
  projectId: 'cashier-8b8c1',
  storageBucket: 'cashier-8b8c1.`appspot.com',
  messagingSenderId: '422015916374',
  appId: '1:422015916374:web:cbd47a849accf75b425ef7',
  measurementId: 'G-J1GDLDJCE7',
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

@Component({
  selector: 'app-cart-details',
  templateUrl: './cart-details.page.html',
  styleUrls: ['./cart-details.page.scss'],
})
export class CartDetailsPage implements OnInit {
  cartItem: any;
  totalPrice: number = 0;
  itemTotal = 1;
  inputName: any;
  isOrderButtonPressed = false;
  isConfirmed = false;
  isOrderLoading = false;

  constructor(
    private cart: CartService,
    private navCtrl: NavController,
    private api: ApiService
  ) {}

  ngOnInit() {
    this.cartItem = this.cart.getCart();
    // this.cartItem = arr;

    this.setTotalPrice();

    setInterval(() => {
      this.setTotalPrice();
    }, 1500);

    console.log(this.cartItem);
    // this.save();
  }

  // async save() {
  //   const fs = require('fs');
  //   var arr = this.cartItem;
  //   await fs.writeFileSync('./FILE.txt', arr.join('\n'));
  // }

  setTotalPrice() {
    this.totalPrice = this.cartItem.reduce(
      (t: any, { price, itemTotal }: any) => t + price * itemTotal,
      0
    );
  }

  addTotalItem = (id: any) => {
    // if (this.itemTotal < 15) this.itemTotal++;
    // console.log(this.itemTotal);
    if (this.cartItem[id - 1].itemTotal < 15) this.cartItem[id - 1].itemTotal++;
    // console.log('id', id);

    // console.log(this.cartItem[id - 1]);

    this.cart.changeCart(this.cartItem);
  };

  minTotalItem = (id: any) => {
    // if (this.itemTotal != 1) this.itemTotal--;
    // console.log(this.itemTotal);
    if (this.cartItem[id - 1].itemTotal != 1) this.cartItem[id - 1].itemTotal--;
    console.log('id', id);

    console.log(this.cartItem[id - 1]);
  };

  pesan() {
    this.isOrderButtonPressed = true;
  }

  confirmOrder(inputName: any) {
    // this.cartItem = this.cart.getCart();
    this.cart.clearCart();

    let newCart = {
      id:
        Math.round(Date.now() + Math.random()) +
        (Math.random() + 1).toString(36).substring(7),
      name: inputName.toLowerCase(),
      isDone: false,
      data: this.cartItem,
    };

    console.log(newCart);

    this.isOrderLoading = true;

    // setTimeout(() => {
    //   this.isConfirmed = true;
    //   this.isOrderLoading = false;
    // }, 2400);

    // console.log(inputName);

    firebase
      .firestore()
      .collection('cart')
      .doc(newCart.name)
      .set(newCart)
      .then((ref) => {
        // console.log('Added doc with ID: ', ref);
        // // success send
        // this.orderLoading = false;

        this.isConfirmed = true;
        this.isOrderLoading = false;
      });
    // .add(newCart)
  }

  backToCart() {
    this.isOrderButtonPressed = false;
    this.isConfirmed = false;
    this.isOrderLoading = false;
  }

  deleteThis(id: any) {
    console.log(this.cartItem);
  }

  backBtn() {
    // this.router.;
    // this.navCtrl.navigateBack('home');
    this.isOrderButtonPressed = false;
    this.navCtrl.back();
  }
}
