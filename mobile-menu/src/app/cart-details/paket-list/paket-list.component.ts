import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-paket-list',
  templateUrl: './paket-list.component.html',
  styleUrls: ['./paket-list.component.scss'],
})
export class PaketListComponent implements OnInit {
  @Input() item: any;
  @Output() plus = new EventEmitter<string>();
  @Output() minus = new EventEmitter<string>();

  constructor() {}

  ngOnInit() {}

  btnPlus(id: string): void {
    this.plus.emit(id);
  }

  btnMinus(id: string): void {
    this.minus.emit(id);
  }
}
