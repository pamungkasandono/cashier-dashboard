import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class CartService {
  cart = [];
  constructor() {}

  getCart() {
    // console.log(this.cart);
    return this.cart;
  }

  setCart(item: any) {
    // this.cart[this.cart.length - 1].id;
    // let a = this.cart[this.cart.length - 1].id;
    // console.log('last', a);

    let index = this.cart.findIndex((x) => x.title === item.title);

    if (index !== -1) {
      this.cart[index].itemTotal++;
    } else {
      this.cart.push(item);

      for (let i in this.cart) {
        this.cart[i].id = parseInt(i) + 1;
        if (typeof this.cart[i].itemTotal == 'undefined') {
          this.cart[i].itemTotal = 1;
        }
      }
    }

    // console.log(item);
  }

  changeCart(item: any) {
    this.cart = [];
    this.cart = item;
  }

  clearCart() {
    this.cart = [];
    // console.log(this.cart.length);
  }
}
