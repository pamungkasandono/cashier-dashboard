import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http: HttpClient, private firestore: AngularFirestore) {}

  get(url: string) {
    return this.http.get(url).toPromise();
  }

  getMakanan() {
    // return this.http.get(
    //   'https://mocki.io/v1/86ce9ef0-f340-4a0f-bada-8e26c476c6d7'
    // );
    return this.firestore.collection('/makanan');
  }

  getMinuman() {
    // return this.http.get(
    //   'https://mocki.io/v1/4073d7d2-d36c-4b93-b7dc-bb026256df74'
    // );
    return this.firestore.collection('/minuman');
  }

  getPaket() {
    // return this.http.get(
    //   'https://mocki.io/v1/1d1c0eed-83c1-45e2-94f0-ad24b7253540'
    // );
    return this.firestore.collection('/paket');
  }

  postCart() {
    // return this.http.get(
    //   'https://mocki.io/v1/29df66b6-601e-4cbe-b8bf-ce2b8e588595'
    // );
  }
}
