import { Component, Input, OnInit, SimpleChange } from '@angular/core';
import { CartService } from 'src/app/services/cart.service';
import { MenuModel } from 'src/app/model/menu.model';
import { BehaviorSubject } from 'rxjs';
import { GetService } from 'src/app/services/get.service';

@Component({
  selector: 'app-makanan',
  templateUrl: './makanan.component.html',
  styleUrls: ['./makanan.component.scss'],
})
export class MakananComponent implements OnInit {
  // private _items = new BehaviorSubject<MenuModel[]>([]);

  @Input() item: MenuModel;
  // @Input() set items(value: MenuModel[]) {
  //   this._items.next(value);
  // }

  // get items() {
  //   return this._items.getValue();
  // }
  // import input for rechieving data from other

  constructor(private cart: CartService, private get: GetService) {}

  ngOnInit() {
    // const start = new Date().getTime();
    // setTimeout(() => {
    //   let searchResults = this.searchCart(this.items, 'p');
    //   console.log('the x', this.items[0]);
    //   console.log('the search', searchResults);
    //   this.items = searchResults;
    //   const end = new Date().getTime();
    //   console.log(`diff time ${end - start} ms`);
    // }, 1000 * 10);
    // this._items.subscribe((x) => {
    //   setTimeout(() => {
    //     console.log('the x', x[0]);
    //     const end = new Date().getTime();
    //     console.log(`diff time ${end - start} ms`);
    //   }, 1000);
    // });
    // setInterval(() => {
    //   console.log('length', x.length);
    //   let a = this.searchCart(x, this.get.setKeyword);
    //   console.log(a);
    //   this._items.next(a);
    // }, 3000);
  }

  // function that recieve value from searchbar component
  getKeywordAndProcessReplaceCurrentLoadedData() {}

  // ngOnChanges(changes: SimpleChange) {
  //   let x = changes['items'].currentValue;
  //   // console.log('value', x);
  //   if (changes['items'].currentValue) {
  //     for (const key in x) {
  //       // console.log('key', key);
  //       // if (Object.prototype.hasOwnProperty.call(x, key)) {
  //       //   const element = x[key];
  //       //   console.log('element', element);
  //       // }
  //     }
  //   }
  // }

  // searchCart(source: any[], keyword: string) {
  //   let results: any[];

  //   keyword = keyword.toUpperCase();
  //   // position = position.toUpperCase(); // not used
  //   results = source.filter(function (entry: { title: string }) {
  //     return entry.title.toUpperCase().indexOf(keyword) !== -1;
  //   });
  //   return results;
  // }

  addToCart(item: any) {
    this.cart.setCart(item);
  }
}
