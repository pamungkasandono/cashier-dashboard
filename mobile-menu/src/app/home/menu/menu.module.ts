import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { MakananComponent } from './makanan/makanan.component';
import { MenuComponent } from './menu.component';
import { MinumanComponent } from './minuman/minuman.component';

@NgModule({
  declarations: [MenuComponent, MakananComponent, MinumanComponent],
  imports: [CommonModule, IonicModule, FormsModule],
  exports: [MenuComponent],
})
export class MenuModule {}
