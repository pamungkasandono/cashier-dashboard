import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { MenuModel } from 'src/app/model/menu.model';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-minuman',
  templateUrl: './minuman.component.html',
  styleUrls: ['./minuman.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MinumanComponent implements OnInit {
  @Input() item: MenuModel;

  constructor(private cart: CartService) {
    // setInterval(() => {
    //   this.ngOnInit();
    // }, 2000);
    // console.log('asd');
  }

  ngOnInit() {}

  addToCart(item: any) {
    this.cart.setCart(item);
  }
}
