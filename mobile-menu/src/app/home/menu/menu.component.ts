import { Component, Input, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { MenuModel } from 'src/app/model/menu.model';
import { ApiService } from 'src/app/services/api.service';
import { GetService } from 'src/app/services/get.service';
import { doSearch } from 'src/app/utilities/other';
import { MenuModule } from './menu.module';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {
  segmentMenu: string = 'makanan';
  bgMakanan: string = '#FFC75F';
  bgMinuman: string = '#FFFFFF';
  borderMakanan: string = '1px solid #FFC75F';
  borderMinuman: string = '1px solid #aeaeae';

  makanan: MenuModel[] = [];
  minuman: MenuModel[] = [];

  makananSaved: MenuModel[] = [];
  minumanSaved: MenuModel[] = [];

  // @Input() recieveKeyword: any;

  private _keyword = new BehaviorSubject<any>([]);

  // @Input() set items(value: any) {
  //   this._items.next(value);
  // }

  @Input() set recieveKeyword(value: any) {
    this._keyword.next(value);
  }

  // get items() {
  //   return this._items.getValue();
  // }

  constructor(private api: ApiService, private get: GetService) {}

  ngOnInit() {
    if (this.makananSaved.length == 0) {
      // console.log('NOT SAVED!', this.makananSaved);
      this.getMakanan();
    }

    if (this.minumanSaved.length == 0) {
      // console.log('NOT SAVED!', this.makananSaved);
      this.getMinuman();
    }

    this._keyword.subscribe((e) => {
      console.log('new data', e);
      if (this.minumanSaved.length) {
        this.minuman = this.searchCart(this.minumanSaved, e.keyword);
      }

      if (this.makananSaved.length) {
        this.makanan = this.searchCart(this.makananSaved, e.keyword);
      }

      if (e) {
        if (e.keyword == '') {
          this.minuman = this.minumanSaved;
          this.makanan = this.makananSaved;
        }
      }
    });

    this.makeSearchbarEmpty();
  }

  // async getMinuman() {
  //   this.api.getMinuman().subscribe((data) => {
  //     let minuman = JSON.stringify(data);
  //     JSON.parse(minuman).data.forEach((element) => {
  //       this.minumanSaved.push({
  //         id: element.id,
  //         category: 'minuman',
  //         title: element.title,
  //         price: element.price,
  //         img: element.img,
  //       });
  //     });
  //     this.minuman = this.minumanSaved.slice();
  //     console.log('this is minuman', this.minuman);
  //   });
  // }

  // async getMakanan() {
  //   this.api.getMakanan().subscribe((data) => {
  //     let makanan = JSON.stringify(data);
  //     JSON.parse(makanan).data.forEach((element) => {
  //       this.makananSaved.push({
  //         id: element.id,
  //         category: 'makanan',
  //         title: element.title,
  //         price: element.price,
  //         img: element.img,
  //       });
  //     });
  //     this.makanan = this.makananSaved.slice();
  //     console.log('this is makanan', this.makanan);
  //   });
  // }

  // async getMinuman() {
  //   this.api.getMinuman().subscribe((data) => {
  //     let minuman = JSON.stringify(data);
  //     JSON.parse(minuman).data.forEach((element) => {
  //       this.minuman.push({
  //         id: element.id,
  //         category: 'minuman',
  //         title: element.title,
  //         price: element.price,
  //         img: element.img,
  //       });
  //     });
  //     // console.log(data['data']);
  //     // this.makanan = data['data'];
  //   });
  // }

  // ------------------------ firebase ------------------------  ///

  async getMinuman() {
    this.api
      .getMinuman()
      .snapshotChanges()
      .subscribe((data) => {
        let makanan = data;
        // console.log(JSON.parse(data).data);
        // this.makanan = JSON.parse(makanan).data;

        data.forEach((element) => {
          element.payload.doc.data()['data'].forEach((e) => {
            this.minumanSaved.push({
              id: e.id,
              category: 'minuman',
              title: e.title,
              price: e.price,
              img: e.img,
            });
          });
        });
        this.minuman = this.minumanSaved.slice();
        console.log('this is minuman', this.minuman);
      });
    // console.log(this.makanan);
  }

  async getMakanan() {
    this.api
      .getMakanan()
      .snapshotChanges()
      .subscribe((data) => {
        let makanan = data;
        // console.log(JSON.parse(data).data);
        // this.makanan = JSON.parse(makanan).data;
        // data.forEach((element) => {
        //   element.payload.doc.data()['data'].forEach((e) => {
        //     this.makanan.push({
        //       id: e.id,
        //       category: 'makanan',
        //       title: e.title,
        //       price: e.price,
        //       img: e.img,
        //     });
        //   });
        // });
        data.forEach((element) => {
          element.payload.doc.data()['data'].forEach((e) => {
            this.makananSaved.push({
              id: e.id,
              category: 'makanan',
              title: e.title,
              price: e.price,
              img: e.img,
            });
          });
        });
        this.makanan = this.makananSaved.slice();
        console.log('this is makanan', this.makanan);
      });
    // console.log(this.makanan);
  }
  // ------------------------ /.firebase ------------------------  ///

  searchCart(source: any[], keyword: string) {
    let results: any[];

    keyword = keyword.toUpperCase();
    // position = position.toUpperCase(); // not used
    results = source.filter(function (entry: { title: string }) {
      return entry.title.toUpperCase().indexOf(keyword) !== -1;
    });
    return results;
  }

  makeSearchbarEmpty() {
    (<HTMLInputElement>document.querySelector('#searchbar')).value = '';
  }

  segmentChange(e: any) {
    // console.log(e);

    if (e.detail.value == 'minuman') {
      console.log('minuman');
      this.bgMinuman = '#F9F871';
      this.bgMakanan = '#FFFFFF';
      this.borderMakanan = '1px solid #aeaeae';
      this.borderMinuman = '1px solid #F9F871';
    } else {
      // console.log('makanan');
      // document.documentElement.style.setProperty(
      //   '--background-checked',
      //   '#FFC75F'
      // );
      this.bgMakanan = '#FFC75F';
      this.bgMinuman = '#FFFFFF';
      this.borderMakanan = '1px solid #FFC751';
      this.borderMinuman = '1px solid #aeaeae';
    }
  }
}
