import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomePageRoutingModule } from './home-routing.module';

import { HomePage } from './home.page';
import { MenuModule } from './menu/menu.module';
import { PaketModule } from './paket/paket.module';
import { SearchbarComponent } from './components/searchbar/searchbar.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
    MenuModule,
    PaketModule,
  ],
  declarations: [HomePage],
})
export class HomePageModule {}
