import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { GetService } from 'src/app/services/get.service';
import { MenuComponent } from '../../menu/menu.component';

@Component({
  providers: [MenuComponent],
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.scss'],
})
export class SearchbarComponent implements OnInit {
  // @Output() msgKeyword = new EventEmitter<string>();

  // sendKeyword(e: any) {
  //   this.msgKeyword.emit(e);
  // }

  constructor(
    private api: ApiService,
    private get: GetService,
    private menuComp: MenuComponent
  ) {}

  ngOnInit() {}

  catchKeyword(e: any) {
    let segment = document.getElementById('segmentPage');
    let position = segment.getAttribute('ng-reflect-ng-switch');
    let now = '';
    if (position == 'menu') {
      let segmentMenu = document.getElementById('segmentMenu');
      let positionMenu = segmentMenu.getAttribute('ng-reflect-ng-switch');
      now = positionMenu;
    } else {
      now = position;
    }

    let searchThing = {
      keyword: e.target.value,
      position: now,
    };

    // return searchThing;

    this.get.setKeyword = searchThing.keyword;

    this.menuComp.ngOnInit();

    switch (searchThing.position) {
      case 'makanan':
        // this.api.getMakanan().subscribe((data) => {
        //   let makananList = JSON.parse(JSON.stringify(data)).data;
        //   let keyword = this.searchCart(makananList, searchThing.keyword);
        //   // console.log(makanan);

        //   JSON.parse(JSON.stringify(data)).data.forEach((element: any) => {
        //     // this.makanan.push({
        //     //   id: element.id,
        //     //   category: 'makanan',
        //     //   title: element.title,
        //     //   price: element.price,
        //     //   img: element.img,
        //     // });
        //   });
        // });
        break;
      case 'minuman':
        // minuman
        break;
      case 'paket':
        // paket
        break;
    }
  }

  searchCart(source: any[], keyword: string) {
    let results: any[];

    keyword = keyword.toUpperCase();
    // position = position.toUpperCase(); // not used
    results = source.filter(function (entry: { title: string }) {
      return entry.title.toUpperCase().indexOf(keyword) !== -1;
    });

    return results;
  }
}
