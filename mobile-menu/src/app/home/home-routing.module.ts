import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import { MenuComponent } from './menu/menu.component';

const routes: Routes = [
  {
    path: '',
    component: HomePage,
    children: [
      {
        path: 'menu',
        // component: MenuComponent,
        children: [
          // {
          //   path: '',
          //   loadChildren: () =>
          //     import('./menu/menu.component').then((m) => m.MenuComponent),
          // },
          {
            path: 'makanan',
            loadChildren: () =>
              import('./menu/makanan/makanan.component') //
                .then((m) => m.MakananComponent),
          },
          {
            path: 'minuman',
            loadChildren: () =>
              import('./menu/minuman/minuman.component') //
                .then((m) => m.MinumanComponent),
          },
        ],
        // [
        //   {
        //     path: '',
        //     loadChildren: () =>
        //       import('./menu/menu.component').then((m) => m.MenuComponent),
        //   },
        // {
        // 	path: ':placeId',
        // 	loadChildren: () =>
        // 		import('./discover/place-detail/place-detail.module').then(
        // 			(m) => m.PlaceDetailPageModule
        // 		)
        // }
        // ],
      },
      {
        path: 'paket',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./paket/paket.component').then((m) => m.PaketComponent),
          },
        ],
      },
    ],
  },
  {
    path: '',
    redirectTo: 'home/menu/makanan',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomePageRoutingModule {}
