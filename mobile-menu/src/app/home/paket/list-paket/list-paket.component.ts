import { Component, Input, OnInit } from '@angular/core';
import { PaketModel } from 'src/app/model/paket.model';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-list-paket',
  templateUrl: './list-paket.component.html',
  styleUrls: ['./list-paket.component.scss'],
})
export class ListPaketComponent implements OnInit {
  @Input() item: PaketModel;

  constructor(private cart: CartService) {}

  ngOnInit() {}

  addToCart(item: any) {
    // console.log(e);
    this.cart.setCart(item);
  }
}
