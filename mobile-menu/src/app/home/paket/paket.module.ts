import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { PaketComponent } from './paket.component';
import { ListPaketComponent } from './list-paket/list-paket.component';

@NgModule({
  declarations: [PaketComponent, ListPaketComponent],
  imports: [CommonModule, IonicModule],
  exports: [PaketComponent],
})
export class PaketModule {}
