import { Component, Input, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { PaketModel } from 'src/app/model/paket.model';
import { ApiService } from 'src/app/services/api.service';
import { doSearch } from 'src/app/utilities/other';

@Component({
  selector: 'app-paket',
  templateUrl: './paket.component.html',
  styleUrls: ['./paket.component.scss'],
})
export class PaketComponent implements OnInit {
  paket: PaketModel[] = [];
  paketSaved: PaketModel[] = [];

  private _keyword = new BehaviorSubject<any>([]);

  @Input() set recieveKeyword(value: any) {
    this._keyword.next(value);
  }

  constructor(private api: ApiService) {}

  ngOnInit() {
    if (this.paketSaved.length == 0) {
      // console.log('NOT SAVED!', this.paketSaved);
      this.getPaket();
    }

    this._keyword.subscribe((e) => {
      console.log('new data', e);
      if (this.paketSaved.length) {
        this.paket = this.searchCart(this.paketSaved, e.keyword);
      }

      if (e) {
        if (e.keyword == '') {
          this.paket = this.paketSaved;
        }
      }
    });
    this.makeSearchbarEmpty();
  }

  makeSearchbarEmpty() {
    (<HTMLInputElement>document.querySelector('#searchbar')).value = '';
  }

  // async getPaket() {
  //   // this.api.getPaket().subscribe((data) => {
  //   // });
  // }

  async getPaket() {
    // this.api.getPaket().subscribe((data) => {
    // });
    /* this.api.getPaket().subscribe((data) => {
      let makanan = JSON.stringify(data);
      // this.makanan = JSON.parse(makanan).data;
      JSON.parse(makanan).data.forEach((element) => {
        this.paketSaved.push({
          id: element.id,
          category: 'paket',
          title: element.title,
          price: element.price,
          item: [
            {
              id: element.item[0].id,
              category: element.item[0].category,
              title: element.item[0].title,
              price: element.item[0].price,
              img: element.item[0].img,
            },
            {
              id: element.item[1].id,
              category: element.item[1].category,
              title: element.item[1].title,
              price: element.item[1].price,
              img: element.item[1].img,
            },
            {
              id: element.item[2].id,
              category: element.item[2].category,
              title: element.item[2].title,
              price: element.item[2].price,
              img: element.item[2].img,
            },
          ],
        });
      });
      this.paket = this.paketSaved.slice();
      console.log('this is paket', this.paket);
    }); */

    this.api
      .getPaket()
      .snapshotChanges()
      .subscribe((data) => {
        // console.log(JSON.parse(data).data);
        // this.makanan = JSON.parse(makanan).data;
        data.forEach((element) => {
          element.payload.doc.data()['data'].forEach((e) => {
            console.log(e);

            this.paketSaved.push({
              id: e.id,
              category: 'paket',
              title: e.title,
              price: e.price,
              item: [
                {
                  id: e.item[0].id,
                  category: e.item[0].category,
                  title: e.item[0].title,
                  price: e.item[0].price,
                  img: e.item[0].img,
                },
                {
                  id: e.item[1].id,
                  category: e.item[1].category,
                  title: e.item[1].title,
                  price: e.item[1].price,
                  img: e.item[1].img,
                },
                {
                  id: e.item[2].id,
                  category: e.item[2].category,
                  title: e.item[2].title,
                  price: e.item[2].price,
                  img: e.item[2].img,
                },
              ],
            });
          });
        });

        this.paket = this.paketSaved.slice();
        console.log('this is paket', this.paket);
      });
    // console.log(this.makanan);
  }

  searchCart(source: any[], keyword: string) {
    let results: any[] = [];
    let entry: any;

    keyword = keyword.toUpperCase();

    for (let i = 0; i < source.length; i++) {
      entry = source[i];
      switch (true) {
        case entry && entry.title.toUpperCase().indexOf(keyword) !== -1:
          results.push(entry);
          break;
        case entry && entry.item[0].title.toUpperCase().indexOf(keyword) !== -1:
          results.push(entry);
          break;
        case entry && entry.item[1].title.toUpperCase().indexOf(keyword) !== -1:
          results.push(entry);
          break;
        case entry && entry.item[2].title.toUpperCase().indexOf(keyword) !== -1:
          results.push(entry);
          break;
      }
    }

    // results = source.filter(function (entry: { title: string, item: any }) {
    //   entry.item[0].title.toUpperCase().indexOf(keyword) !== -1;
    //   return entry.title.toUpperCase().indexOf(keyword) !== -1;
    // });
    return results;
  }
}
