import { Component, Input, OnInit } from '@angular/core';
import { callThisToLoggingAnArt, doSearch } from '../utilities/other';
import { CartService } from '../services/cart.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  segment: string = 'menu';
  image: String = './assets/icon/smiling-face-with-smiling-eyes.png';
  cartLength: number = this.cart.getCart().length;

  keyword: any;

  constructor(private cart: CartService) {
    setInterval(() => {
      this.cartLength = this.cart.getCart().length;
    }, 1500);
  }

  ngOnInit() {}

  changePage(e: any) {
    // let searchbar = document.getElementById('searchbar')['value'];
    // if (searchbar != '') {
    //   doSearch(searchbar);
    // }
    // callThisToLoggingAnArt();
    // console.log(e.detail);
    // this.segment = e.detail.value;
  }

  catchKeyword(e: any) {
    let segment = document.getElementById('segmentPage');
    let position = segment.getAttribute('ng-reflect-ng-switch');
    let now = '';
    if (position == 'menu') {
      let segmentMenu = document.getElementById('segmentMenu');
      let positionMenu = segmentMenu.getAttribute('ng-reflect-ng-switch');
      now = positionMenu;
    } else {
      now = position;
    }

    let searchThing = {
      keyword: e.target.value,
      position: now,
    };

    // console.log(searchThing);
    this.keyword = searchThing;
  }

  openCart() {
    this.cart.getCart();
  }
}
