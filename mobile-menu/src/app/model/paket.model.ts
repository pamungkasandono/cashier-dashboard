import { MenuModel } from './menu.model';

export interface PaketModel {
  id: number;
  category: string;
  title: string;
  price: number;
  item: MenuModel[];
}
