export interface MenuModel {
  id: number;
  category: string;
  title: string;
  price: number;
  img: string;
}
