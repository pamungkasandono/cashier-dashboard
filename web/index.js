import firebase from "firebase/compat/app";
import "firebase/compat/firestore";
import express from "express";
const firebaseConfig = {
    apiKey: "AIzaSyC-2JKlYnAEHdU4v5gJe3XXkWLG1xLxuc0",
    authDomain: "cashier-8b8c1.firebaseapp.com",
    projectId: "cashier-8b8c1",
    storageBucket: "cashier-8b8c1.`appspot.com",
    messagingSenderId: "422015916374",
    appId: "1:422015916374:web:cbd47a849accf75b425ef7",
    measurementId: "G-J1GDLDJCE7",
};
firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();

const app = express();

app.listen("8000", (req, res) => {
    console.log("Tes");
});

app.set("views", "./views");
app.set("view engine", "ejs");

app.use(express.static("./styles"));

app.get("/", async (req, res) => {
    const request = await db.collection("cart").get();
    const listMeja = request.docs.map((doc) => ({
        id: doc.id,
        data: doc.data(),
    }));

    let data = getByValue(listMeja, req.query.table) || [];

    let dataSaved;
    if (data.length != 0) {
        dataSaved = data;
        data = data.data.data || [];
    } else {
        data = [];
    }
    if (req.query.docId) {
        let newData = { ...dataSaved.data };
        newData.isDone = true;
        await db.collection("cart").doc(req.query.docId).set(newData);
    }
    let dataid;
    if (dataSaved) {
        dataid = dataSaved.id || "first";
    }
    res.render("index", { listMeja, data, dataid });
});

function getByValue(arr, value) {
    let indexPos = arr
        .map(function (e) {
            return e.id;
        })
        .indexOf(value);

    return arr[indexPos];
}
